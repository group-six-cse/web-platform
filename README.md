 # Business Directory and Appointment Booking Platform

This platform allows businesses to create accounts and showcase their offerings, while customers can browse and book appointments or place orders.

## Features
### Business Account

- Create an account with basic information (name, contact details, location, description, etc.)
- Upload logos, photos, and videos to showcase offerings
- Manage menus, service listings, or appointment slots
- Set pricing and availability for offerings

 ## Customer Functionality 
Browse businesses by category, location, or keywords- View detailed business profiles, menus, service descriptions, or appointment schedules- Place orders for online delivery or pickup- Book appointments for haircuts, massages, or fitness classes- Leave reviews and ratings for businesses

## Admin panel
 
• Manage user accounts (businesses and customers)
•Monitor platform activity (orders, bookings, reviews)
•Generate reports and analytics for businesses.
•Manage platform settings and configurations.

 ## Technical Requirements

- Backend: Django framework for server-side development
- Database: PostgreSQL or MySQL for storing user, business, and service data
- Email API: SendGrid or Mailgun for order confirmations, booking reminders, etc.
## authors
WAMALA ABDUL JUNIOR -23/U/18475/EVE 

MATOVU JOEL         -23/U/11108/EVE

AMONG TRACY ANGELA  -23/U/23871/EVE

KISIRINYA SIRAJJE   -23/U/24072/EVE

KANYESIGYE NATHAN   - 23/U/09008/EVE
