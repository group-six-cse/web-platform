from django.contrib import admin

from .models import *
admin.site.register(Product)

admin.site.register(Categories)

admin.site.register(Login)

admin.site.register(Profile)

admin.site.register(OrderItems)

admin.site.register(RegisterBusiness)

admin.site.register(Customer)

admin.site.register(Order)

admin.site.register(Cart)

admin.site.register(CartItem)


# Register your models here.
