# Generated by Django 5.0.3 on 2024-03-20 15:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shophouse', '0006_remove_categories_description_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='title',
        ),
        migrations.AddField(
            model_name='product',
            name='name',
            field=models.CharField(default='', max_length=25),
        ),
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.ImageField(upload_to='shophouse/media/'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.IntegerField(),
        ),
    ]
